vim.keymap.set('n', '<leader><leader>x', "<Plug>JupyterExecute")
vim.keymap.set('n', '<leader><leader>X', "<Plug>JupyterExecuteAll")
vim.keymap.set('n', '<leader><leader>r', "<Plug>JupyterRestart")
