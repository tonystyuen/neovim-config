local lsp_zero = require("lsp-zero")

lsp_zero.preset("recommended")

lsp_zero.ensure_installed({
  'rust_analyzer',
  'clangd',
  'pyright',
})

-- local cmp = require('cmp')
-- local cmp_select = {behavior = cmp.SelectBehavior.Select}
-- local cmp_mappings = lsp_zero.defaults.cmp_mappings({
--   ['<C-p>'] = cmp.mapping.select_prev_item(cmp_select),
--   ['<C-n>'] = cmp.mapping.select_next_item(cmp_select),
--   ['<C-y>'] = cmp.mapping.confirm({ select = true }),
--   ['<C-x>'] = cmp.mapping.close(),
--   ["<C-Space>"] = cmp.mapping.complete(),
-- })

lsp_zero.set_preferences({
  sign_icons = {}
})

lsp_zero.setup_nvim_cmp({
  -- mapping = cmp_mappings
  preselect = 'none',
  completion = {
    completeopt = 'menu,menuone,noinsert,noselect'
  },
})

lsp_zero.on_attach(function(client, bufnr)
  vim.keymap.set("n", "gd", function() vim.lsp.buf.definition() end,
    { buffer = bufnr, remap = false, desc = "[G]oto [D]efinition" })
  vim.keymap.set("n", "K", function() vim.lsp.buf.hover() end,
    { buffer = bufnr, remap = false, desc = "Show signature in floating window" })
  vim.keymap.set("n", "]d", function() vim.diagnostic.goto_next() end,
    { buffer = bufnr, remap = false, desc = "Show next diagnostic" })
  vim.keymap.set("n", "[d", function() vim.diagnostic.goto_prev() end,
    { buffer = bufnr, remap = false, desc = "Show previous diagnostic" })
  vim.keymap.set("n", "<leader>ca", function() vim.lsp.buf.code_action() end,
    { buffer = bufnr, remap = false, desc = "[C]ode [A]ction" })
  vim.keymap.set("n", "<leader>fr", function() vim.lsp.buf.references() end,
    { buffer = bufnr, remap = false, desc = "[F]ind [R]eferences" })
  vim.keymap.set("n", "<leader>rn", function() vim.lsp.buf.rename() end,
    { buffer = bufnr, remap = false, desc = "[R]e-[N]ame" })
  vim.keymap.set("i", "<C-h>", function() vim.lsp.buf.signature_help() end,
    { buffer = bufnr, remap = false, desc = "Show signature help" })
end)

lsp_zero.setup()
