-- Enable project local settings, load cd's .exrc, .vimrc/.nvimrc
vim.o.exrc = false
require("exrc").setup({
  file = {
    "nvimrc.lua",
  },
})

