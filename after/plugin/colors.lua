require('onedark').setup {
  -- Main options --
  transparent = true,

  -- Lualine options --
  lualine = {
    transparent = false, -- lualine center bar transparency
  },
}
require('onedark').load()
