-- examples for your init.lua

-- disable netrw at the very start of your init.lua (strongly advised)
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

vim.keymap.set("n", "<leader>nt", ":NvimTreeToggle<CR>", { desc = "[N]vimTree [T]oggle" })
vim.keymap.set("n", "<leader>nf", ":NvimTreeFocus<CR>", { desc = "[N]vimTree [F]ocus" })

-- OR setup with some options
require("nvim-tree").setup({
  sort_by = "case_sensitive",
  renderer = {
    group_empty = true,
  },
  filters = {
    dotfiles = true,
  },
  actions = {
    open_file = {
      quit_on_open = false,
      window_picker = {
        exclude = {
          filetype = { "notify", "packer", "qf", "diff", "fugitive", "fugitiveblame" },
          -- Enable selection for terminal
          -- buftype = { "nofile", "terminal", "help" },
          buftype = {},
        },
      },
    },
  },
  view = { relativenumber = true },
})
