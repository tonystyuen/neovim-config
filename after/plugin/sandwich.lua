vim.g.sandwich_no_default_key_mappings = 1

-- add
vim.keymap.set({ "n", "x", "o" }, "<leader>sa", '<Plug>(sandwich-add)')

-- delete
vim.keymap.set({ "n", "x" }, "<leader>sd", '<Plug>(sandwich-delete)')
vim.keymap.set({ "n" }, "<leader>sdb", '<Plug>(sandwich-delete-auto)')

-- replace
vim.keymap.set({ "n", "x" }, "<leader>sr", '<Plug>(sandwich-replace)')
vim.keymap.set({ "n" }, "<leader>srb", '<Plug>(sandwich-replace-auto)')
