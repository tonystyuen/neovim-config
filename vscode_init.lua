-- Set leader key
vim.g.mapleader = " "
vim.g.maplocalleader= " "

-- Cursor stay in the middle while navigating
vim.keymap.set("n", "<C-d>", "<C-f>zz")
vim.keymap.set("n", "<C-u>", "<C-b>zz")

-- Cursor stay in middle where searching
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

-- Move selected block
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

-- Paste wihtout overwriting register
vim.keymap.set("x", "<leader>p", "\"_dP")

-- Copy to system clipboard
vim.keymap.set("n", "<leader>y", "\"+y")
vim.keymap.set("v", "<leader>y", "\"+y")
vim.keymap.set("n", "<leader>Y", "\"+Y")

-- Delete to void register
vim.keymap.set("n", "<leader>d", "\"_d")
vim.keymap.set("v", "<leader>d", "\"_d")

-- Install packer
local install_path = vim.fn.stdpath 'data' .. '/site/pack/packer/start/packer.nvim'
local is_bootstrap = false
if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
  is_bootstrap = true
  vim.fn.system { 'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path }
  vim.cmd [[packadd packer.nvim]]
end

require('packer').startup(function(use)
  -- Package manager
  use 'wbthomason/packer.nvim'

  use 'unblevable/quick-scope'

  use 'machakann/vim-sandwich'

  if is_bootstrap then
    require('packer').sync()
  end
end)

-- Quick scope settings vscode spefic
vim.api.nvim_set_hl(0,'QuickScopePrimary', { fg='#afff5f', underline=true, ctermfg=155, cterm={ underline } }) 
vim.api.nvim_set_hl(0,'QuickScopeSecondary', { fg='#5fffff', underline=true, ctermfg=81, cterm={ underline } }) 
-- Quick scope settings
vim.g.qs_highlight_on_keys = {'f', 'F', 't', 'T'}

-- Vim sandwich settings
vim.api.nvim_set_hl(0, 'OperatorSandwichBuns', { fg='#aa91a0', underline=true, ctermfg=172, cterm={ underline } })
vim.api.nvim_set_hl(0, 'OperatorSandwichChange', { fg='#edc41f', underline=true, ctermfg='yellow', cterm={ underline } })
vim.api.nvim_set_hl(0, 'OperatorSandwichAdd', { fg='#b1fa87', underline=true, ctermfg='green', cterm={ none } })
vim.api.nvim_set_hl(0, 'OperatorSandwichDelete', { fg='#cf5963', ctermfg='red', cterm={ none } })
